@echo off
copy /b/v/y ..\..\AzurLaneData\en-US\sharecfg\aircraft_template.lua aircraft_template.lua
copy /b/v/y ..\..\AzurLaneData\en-US\sharecfg\chapter_template.lua chapter_template.lua
copy /b/v/y ..\..\AzurLaneData\en-US\sharecfg\chapter_template_loop.lua chapter_template_loop.lua
copy /b/v/y ..\..\AzurLaneData\en-US\sharecfg\enemy_data_skill.lua enemy_data_skill.lua
copy /b/v/y ..\..\AzurLaneData\en-US\sharecfg\enemy_data_statistics.lua enemy_data_statistics.lua
copy /b/v/y ..\..\AzurLaneData\en-US\sharecfg\ship_data_statistics.lua ship_data_statistics.lua
copy /b/v/y ..\..\AzurLaneData\en-US\sharecfg\ship_skin_template.lua ship_skin_template.lua
copy /b/v/y ..\..\AzurLaneData\en-US\sharecfg\weapon_property.lua weapon_property.lua
copy /b/v/y ..\..\AzurLaneData\en-US\sharecfg\equip_data_statistics.lua equip_data_statistics.lua
copy /b/v/y ..\..\AzurLaneData\en-US\sharecfg\ship_skin_words.lua ship_skin_words.lua
