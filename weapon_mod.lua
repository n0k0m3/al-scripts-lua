require("pg.weapon_property")
require("pg.equip_data_statistics")
local function readConfig(file)
  local f = io.open(file, "r")
  local content = f:read("*all")
  f:close()
  local func = loadstring(content)
  func()
end
local mod_config_path = "D:\\AZL\\al-scripts-lua\\ALScripts\\"
--- Module Starts here ---
readConfig(mod_config_path .. "weapon_config.lua")
-- Functions --
local function generate_weapon_list()
  local equip_base = {}
  for k, v in pairs(pg.equip_data_statistics) do
    if k ~= "all" then
      local base = v.base
      local weapon_table = v.weapon_id
      if base ~= nil then
        local type_1 = pg.equip_data_statistics[base].type
        if type_1 ~= 0 and weapon_table ~= nil and weapon_table[1] ~= nil then
          local type_1 = pg.equip_data_statistics[base].type
          equip_base[type_1] = equip_base[type_1] or {}
          equip_base[type_1][base] = equip_base[type_1][base] or {}
          table.insert(equip_base[type_1][base], k)
        end
      else
        local name = v.name
        if name:find("Default") then
          local type_1 = v.type
          equip_base[type_1] = equip_base[type_1] or {}
          equip_base[type_1][k] = equip_base[type_1][k] or {}
        end
      end
    end
  end
  for type_1, base in pairs(equip_base) do
    for b, equip in pairs(base) do
      table.insert(equip, b)
    end
  end

  local weapon_list = {}
  for type1, v in pairs(equip_base) do
    weapon_list[type1] = weapon_list[type1] or {}
    for k1, v1 in pairs(v) do
      for k2, v2 in pairs(v1) do
        for k3, v3 in pairs(pg.equip_data_statistics[v2].weapon_id) do
          table.insert(weapon_list[type1], v3)
        end
      end
    end
  end
  return weapon_list
end

local function apply_weapon_mod(weapon_list)
  for type1, v in pairs(weapon_stats) do
    local enable = v.enable
    if enable then
      for k, weapon_id in pairs(weapon_list[type1]) do
        pg.weapon_property[weapon_id].damage = v.damage or pg.weapon_property[weapon_id].damage
        pg.weapon_property[weapon_id].reload_max = v.reload_max or pg.weapon_property[weapon_id].reload_max
      end
    end
  end
end
-- Main Program --
local weapon_list = generate_weapon_list()
local modding_weapon = apply_weapon_mod(weapon_list)