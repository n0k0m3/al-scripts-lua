--enable to mod, set nil for stats you don't want to mod
weapon_stats = {
  --Artillery (Destroyer)
  [1] = {
    enable = true,
    damage = 386,
    reload_max = 48
  },
  --Artillery (Light Cruiser)
  [2] = {
    enable = true,
    damage = 386,
    reload_max = 48
  },
  --Artillery (Heavy Cruiser)
  [3] = {
    enable = true,
    damage = 386,
    reload_max = 48
  },
  --Artillery (Battleship)
  [4] = {
    enable = false,
    damage = 386,
    reload_max = 48
  },
  --Artillery (Pocket Battleship)
  [11] = {
    enable = false,
    damage = 386,
    reload_max = 48
  },
  --Anti-Air Gun
  [6] = {
    enable = true,
    damage = 386,
    reload_max = 48
  },
  --Torpedo
  [5] = {
    enable = true,
    damage = 386,
    reload_max = 48
  },
  --Torpedo (Submarine)
  [13] = {
    enable = true,
    damage = 386,
    reload_max = 48
  },
  --Fighter
  [7] = {
    enable = true,
    damage = 386,
    reload_max = 48
  },
  --Torpedo Bomber
  [8] = {
    enable = true,
    damage = 386,
    reload_max = 48
  },
  --Dive Bomber
  [9] = {
    enable = true,
    damage = 386,
    reload_max = 48
  },
  --Reconnaissance
  [12] = {
    enable = true,
    damage = 386,
    reload_max = 48
  },
  --Anti-submarine aircraft
  [15] = {
    enable = true,
    damage = 386,
    reload_max = 48
  },
  --Auxiliary (ASW) (Depth Charges)
  [14] = {
    enable = true,
    damage = 386,
    reload_max = 48
  },
  --Non-descriptive Aircraft Carrier Airstrike
  [99] = {
    enable = false,
    damage = 386,
    reload_max = 48
  }
}
