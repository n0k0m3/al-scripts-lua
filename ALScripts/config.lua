module("_mod", package.seeall)
login_tips = "[From n0k0m3]Your account has been frozen. Please contact customer support[memetips]"
--"Modded scripts are free. If you paid for it, ask for a refund.\n Author: moritaka & n0k0m3"
-- Global Mod
enable_global = true
error_50 = false

-- Modules
---- Included Details Config
aircraft_mod = true
godmode = true
weakenemy = true
fast_map_movement = false
---- Separated Details Config
skin_mod = true
weapon_mod = true
---- No Detail Config
remove_skill = true
remove_hard_mode_limit = false


-- Included Details Config (set 'nil' for unchanged)
---- Aircraft (with min value default)
max_hp = nil -- min 13
hp_growth = nil -- min 0
accuracy = 1 -- min 23
ACC_growth = 1 -- min 0
attack_power = 1 -- min 1
AP_growth = 1 -- min 2000
crash_DMG = 1 -- min 1
speed = nil -- min 20
---- Godmode
antiaircraft = 0
antiaircraft_growth = 0
antisub = 0
armor = nil
armor_growth = nil
cannon = nil
cannon_growth = nil
dodge = 0
dodge_growth = 0
hit = 0
hit_growth = 0
luck = 0
luck_growth = 0
reload = 1
reload_growth = 0
speed = 20
speed_growth = 0
torpedo = 0
torpedo_growth = 0
---- Weakenemy
durability = 50
durability_growth = 0
---- Fast map movement
movespeed = 0.1 -- default 0.5