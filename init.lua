for filename in io.popen('dir /b/a-d "pg"'):lines() do
  filename = filename:match "^(.*)%.lua$"
  if filename then
    require("pg." .. filename)
  end
end

-----------------Generate Weapon List----------------------------------
function generate_weapon_list()
  local equip_base = {}
  for k, v in pairs(pg.equip_data_statistics) do
    if k ~= "all" then
      local base = v.base
      local weapon_table = v.weapon_id
      if base ~= nil then
        local type_1 = pg.equip_data_statistics[base].type
        if type_1 ~= 0 and weapon_table ~= nil and weapon_table[1] ~= nil then
          local type_1 = pg.equip_data_statistics[base].type
          equip_base[type_1] = equip_base[type_1] or {}
          equip_base[type_1][base] = equip_base[type_1][base] or {}
          table.insert(equip_base[type_1][base], k)
        end
      else
        local name = v.name
        if name:find("Default") then
          local type_1 = v.type
          equip_base[type_1] = equip_base[type_1] or {}
          equip_base[type_1][k] = equip_base[type_1][k] or {}
        end
      end
    end
  end
  for type_1, base in pairs(equip_base) do
    for b, equip in pairs(base) do
      table.insert(equip, b)
    end
  end

  local weapon_list = {}
  for type1, v in pairs(equip_base) do
    weapon_list[type1] = weapon_list[type1] or {}
    for k1, v1 in pairs(v) do
      for k2, v2 in pairs(v1) do
        for k3, v3 in pairs(pg.equip_data_statistics[v2].weapon_id) do
          table.insert(weapon_list[type1], v3)
        end
      end
    end
  end
  return weapon_list
end
weapon_list = generate_weapon_list()
------------------Library----------------------------------------------
local function readConfig(file)
  local f = io.open(file, "r")
  local content = f:read("*all")
  f:close()
  local func = loadstring(content)
  func()
end

------------------Functions--------------------------------------------
local function err_50()
  if _mod.error_50 then
    function ys.Battle.BattleState.Vertify()
      return true, -1
    end
  end
end

local function prevent_EX()
  for k, v in pairs(pg.chapter_template) do
    if (v.chapter_name == "EX" or v.chapter_name == "EXTRA") then
      v.unlocklevel = 150 -- Basically, if you use mod, you can't play EX.
    end
  end
end

-- Modular Modding --
local function weapon_mod(_mod, mod_config_path)
  if _mod.weapon_mod then
    readConfig(mod_config_path .. "weapon_config.lua")
    -- Functions --
    local function apply_weapon_mod(weapon_list)
      for type1, v in pairs(weapon_stats) do
        local enable = v.enable
        if enable then
          for k, weapon_id in pairs(weapon_list[type1]) do
            if pg.weapon_property[weapon_id] ~= nil then
              pg.weapon_property[weapon_id].damage = v.damage or pg.weapon_property[weapon_id].damage
              pg.weapon_property[weapon_id].reload_max = v.reload_max or pg.weapon_property[weapon_id].reload_max
            end
          end
        end
      end
    end
    -- Main Program --
    local modding_weapon = apply_weapon_mod(weapon_list)
    --  if _mod.damage or _mod.cooldown then
    --    for k, v in pairs(pg.weapon_property) do
    --      if tostring(k) ~= "all" then
    --        if _mod.damage then
    --          v.damage = _mod._damage or v.damage
    --        end
    --        if _mod.cooldown and _mod.no_bb then
    --          if v.type ~= 23 then
    --            v.reload_max = _mod.reload_max or v.reload_max
    --          end
    --        elseif _mod.cooldown and not _mod.no_bb then
    --          v.reload_max = _mod.reload_max or v.reload_max
    --        end
    --      end
    --    end
    --  end
  end
end

local function godmode_weakenemy(_mod)
  if _mod.godmode or _mod.weakenemy then
    for k, v in pairs(pg.enemy_data_statistics) do
      if tostring(k) ~= "all" then
        if _mod.godmode then
          v.equipment_list = {}
          v.antiaircraft = _mod.antiaircraft or v.antiaircraft
          v.antiaircraft_growth = _mod.antiaircraft_growth or v.antiaircraft_growth
          v.antisub = _mod.antisub or v.antisub
          v.armor = _mod.armor or v.armor
          v.armor_growth = _mod.armor_growth or v.armor_growth
          v.cannon = _mod.cannon or v.cannon
          v.cannon_growth = _mod.cannon_growth or v.cannon_growth
          v.dodge = _mod.dodge or v.dodge
          v.dodge_growth = _mod.dodge_growth or v.dodge_growth
          v.hit = _mod.hit or v.hit
          v.hit_growth = _mod.hit_growth or v.hit_growth
          v.luck = _mod.luck or v.luck
          v.luck_growth = _mod.luck_growth or v.luck_growth
          v.reload = _mod.reload or v.reload
          v.reload_growth = _mod.reload_growth or v.reload_growth
          v.speed = _mod.speed or v.speed
          v.speed_growth = _mod.speed_growth or v.speed_growth
          v.torpedo = _mod.torpedo or v.torpedo
          v.torpedo_growth = _mod.torpedo_growth or v.torpedo_growth
        end
        if _mod.weakenemy then
          v.durability = _mod.durability or v.durability
          v.durability_growth = _mod.durability_growth or v.durability_growth
        end
      end
    end
  end
end

local function aircraft_mod(_mod)
  if _mod.aircraft_mod then
    for k, v in pairs(pg.aircraft_template) do
      if tostring(k) ~= "all" then
        v.max_hp = _mod.max_hp or v.max_hp
        v.hp_growth = _mod.hp_growth or v.hp_growth
        v.accuracy = _mod.accuracy or v.accuracy
        v.ACC_growth = _mod.ACC_growth or v.ACC_growth
        v.attack_power = _mod.attack_power or v.attack_power
        v.AP_growth = _mod.AP_growth or v.AP_growth
        v.crash_DMG = _mod.crash_DMG or v.crash_DMG
        v.speed = _mod.speed or v.speed
      end
    end
  end
end

local function fast_map_movement(_mod)
  if _mod.fast_map_movement then
    ChapterConst.ShipStepDuration = _mod.movespeed
  end
end

local function skin_mod(mod_config_path, _mod)
  if _mod.skin_mod then
    -- Functions --
    local function generate_skinData(dataFile, skinSettings)
      local skinData = {}
      for k, v in pairs(pg.ship_skin_template.get_id_list_by_ship_group) do
        if tonumber(k) <= 900000 or tonumber(k) >= 901000 then
          if table.getn(v) > 1 then
            local shipId = k * 10 + 1
            skinData[shipId] = {}
            for k1, skinId in pairs(v) do
              if skinId ~= shipId - 1 then
                local skinName = pg.ship_skin_template[skinId].name
                local skin_showing = pg.ship_skin_template[skinId].no_showing
                if skin_showing ~= "1" then
                  skinData[shipId][skinId] = skinName
                end
              end
            end
          end
        end
      end
      local shipIdTable = {}
      for shipId, v0 in pairs(skinData) do
        table.insert(shipIdTable, shipId)
      end
      table.sort(shipIdTable)
      local f = io.open(dataFile, "w+")
      for k, shipId in pairs(shipIdTable) do
        local v0 = skinData[shipId]
        local shipname = pg.ship_data_statistics[shipId].english_name
        f:write("[" .. shipname .. "]\n")
        local skinIdTable = {}
        for k1, v1 in pairs(v0) do
          table.insert(skinIdTable, k1)
        end
        table.sort(skinIdTable)
        for k2, v2 in pairs(skinIdTable) do
          local skinStatus
          local skinName = v0[v2]
          local skin_type = pg.ship_skin_template[v2].skin_type
          if skin_type == 1 then
            skinName = string.format("%s", skinName:match("^%s*(.-)%s*$")) .. " (Wedding)"
          elseif skin_type == 4 then
            skinName = string.format("%s", skinName:match("^%s*(.-)%s*$")) .. " (Blu-ray/Event Limited)"
          end
          if skinSettings ~= nil then
            local skinEnable = skinSettings[v2]
            if skinEnable then
              skinStatus = "+"
            else
              skinStatus = "-"
            end
          else
            skinStatus = "-"
          end
          f:write(skinStatus .. v2 .. ":" .. skinName .. "\n")
        end
        f:write("\n")
      end
      f:close()
    end

    local function read_skinData(dataFile)
      if io.open(dataFile) == nil then
        return nil
      end
      local count = 0
      local skinSettings = {}
      for line in io.lines(dataFile) do
        if string.sub(line, 1, 1) == "+" then
          local u, v = string.find(line, ":")
          skinSettings[tonumber(string.sub(line, 2, u - 1))] = true
        end
      end
      if next(skinSettings) == nil then
        skinSettings = nil
      end
      return skinSettings
    end

    local function apply_skin(skinSettings)
      if skinSettings ~= nil then
        local shipGroup_skinid_table = {}
        local shipGroup_shipid_table = {}
        for skinId, v in pairs(skinSettings) do
          local shipGroup = pg.ship_skin_template[skinId].ship_group
          shipGroup_skinid_table[shipGroup] = skinId
        end
        for shipId, v in pairs(pg.ship_data_statistics) do
          if tostring(shipId) ~= "all" then
            local default_skinid = v.skin_id
            local shipinfo = pg.ship_skin_template[default_skinid]
            if shipinfo ~= nil then
              local shipGroup = pg.ship_skin_template[default_skinid].ship_group
              shipGroup_shipid_table[shipGroup] = shipGroup_shipid_table[shipGroup] or {}
              table.insert(shipGroup_shipid_table[shipGroup], shipId)
            end
          end
        end
        for shipGroup, skinId in pairs(shipGroup_skinid_table) do
          for k, shipId in pairs(shipGroup_shipid_table[shipGroup]) do
            if (math.floor((shipId % 100000) / 1000) ~= 99) or (shipId % 10 ~= 1) then -- check if ship is PR
              pg.ship_data_statistics[shipId].skin_id = skinId
            end
          end
        end
      end
    end

    local function fix_skin_word(skinSettings)
      if skinSettings ~= nil then
        for skinId, v in pairs(skinSettings) do
          local shipGroup = pg.ship_skin_template[skinId].ship_group
          for k, v in pairs(pg.ship_skin_words[skinId]) do
            if v == "" or (type(v) == "table" and v[1] == nil) then
              pg.ship_skin_words[skinId][k] = pg.ship_skin_words[shipGroup * 10][k]
            end
          end
        end
      end
    end

    -- Main Program --
    local dataFile = mod_config_path .. "skin.ini"
    local skinSettings_pre = read_skinData(dataFile)
    local run = generate_skinData(dataFile, skinSettings_pre)
    skinSettings_pre = nil
    local skinSettings = read_skinData(dataFile)
    local applyskin = apply_skin(skinSettings)
    local fixskinword = fix_skin_word(skinSettings)
  end
end

-- Non-modular Modding --
local function remove_skill(_mod)
  if _mod.remove_skill then
    for k, v in pairs(pg.enemy_data_skill) do
      if tostring(k) ~= "all" then
        v.is_repeat = 0
        v.skill_list = {}
      end
    end
  end
end

local function remove_hard_mode_limit(_mod)
  if _mod.remove_hard_mode_limit then
    for k, v in pairs(pg.chapter_template) do
      if tostring(k) ~= "all" then
        v.limitation = {{{0, 0, 0}, {0, 0, 0}}, {{0, 0, 0}, {0, 0, 0}}}
        v.property_limitation = {}
      end
    end
    for k, v in pairs(pg.chapter_template_loop) do
      if tostring(k) ~= "all" then
        v.limitation = {{{0, 0, 0}, {0, 0, 0}}, {{0, 0, 0}, {0, 0, 0}}}
        v.property_limitation = {}
      end
    end
  end
end

------------------Load Config-------------------------------------------
local mod_config_path = "D:\\AZL\\al-scripts-lua\\ALScripts\\"
readConfig(mod_config_path .. "config.lua")
-- pg.gametip.login_loginMediator_loginSuccess["tip"] = _mod.login_tips
-------------------Main-------------------------------------------------
if _mod.enable_global then
  err_50()
  prevent_EX()
  weapon_mod(_mod, mod_config_path)
  godmode_weakenemy(_mod)
  aircraft_mod(_mod)
  fast_map_movement(_mod)
  skin_mod(mod_config_path, _mod)
  remove_skill(_mod)
  remove_hard_mode_limit(_mod)
end
-------------------End--------------------------------------------------