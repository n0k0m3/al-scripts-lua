require("pg.ship_skin_template")
require("pg.ship_data_statistics")
require("pg.ship_skin_words")
local mod_config_path = ""
--- Module Starts here ---
-- Functions --
local function generate_skinData(dataFile, skinSettings)
  local skinData = {}
  for k, v in pairs(pg.ship_skin_template.get_id_list_by_ship_group) do
    if tonumber(k) <= 900000 or tonumber(k) >= 901000 then
      if table.getn(v) > 1 then
        local shipId = k * 10 + 1
        skinData[shipId] = {}
        for k1, skinId in pairs(v) do
          if skinId ~= shipId - 1 then
            local skinName = pg.ship_skin_template[skinId].name
            local skin_showing = pg.ship_skin_template[skinId].no_showing
            if skin_showing ~= "1" then
              skinData[shipId][skinId] = skinName
            end
          end
        end
      end
    end
  end
  local shipIdTable = {}
  for shipId, v0 in pairs(skinData) do
    table.insert(shipIdTable, shipId)
  end
  table.sort(shipIdTable)
  local f = io.open(dataFile, "w+")
  for k, shipId in pairs(shipIdTable) do
    local v0 = skinData[shipId]
    local shipname = pg.ship_data_statistics[shipId].english_name
    f:write("[" .. shipname .. "]\n")
    local skinIdTable = {}
    for k1, v1 in pairs(v0) do
      table.insert(skinIdTable, k1)
    end
    table.sort(skinIdTable)
    for k2, v2 in pairs(skinIdTable) do
      local skinStatus
      local skinName = v0[v2]
      local skin_type = pg.ship_skin_template[v2].skin_type
      if skin_type == 1 then
        skinName = string.format("%s", skinName:match("^%s*(.-)%s*$")) .. " (Wedding)"
      elseif skin_type == 4 then
        skinName = string.format("%s", skinName:match("^%s*(.-)%s*$")) .. " (Blu-ray/Event Limited)"
      end
      if skinSettings ~= nil then
        local skinEnable = skinSettings[v2]
        if skinEnable then
          skinStatus = "+"
        else
          skinStatus = "-"
        end
      else
        skinStatus = "-"
      end
      f:write(skinStatus .. v2 .. ":" .. skinName .. "\n")
    end
    f:write("\n")
  end
  f:close()
end
local function read_skinData(dataFile)
  if io.open(dataFile) == nil then
    return nil
  end
  local count = 0
  local skinSettings = {}
  for line in io.lines(dataFile) do
    if string.sub(line, 1, 1) == "+" then
      local u, v = string.find(line, ":")
      skinSettings[tonumber(string.sub(line, 2, u - 1))] = true
    end
  end
  if next(skinSettings) == nil then
    skinSettings = nil
  end
  return skinSettings
end

local function apply_skin(skinSettings)
  if skinSettings ~= nil then
    local shipGroup_skinid_table = {}
    local shipGroup_shipid_table = {}
    for skinId, v in pairs(skinSettings) do
      local shipGroup = pg.ship_skin_template[skinId].ship_group
      shipGroup_skinid_table[shipGroup] = skinId
    end
    for shipId, v in pairs(pg.ship_data_statistics) do
      if tostring(shipId) ~= "all" then
        local default_skinid = v.skin_id
        local shipinfo = pg.ship_skin_template[default_skinid]
        if shipinfo ~= nil then
          local shipGroup = pg.ship_skin_template[default_skinid].ship_group
          shipGroup_shipid_table[shipGroup] = shipGroup_shipid_table[shipGroup] or {}
          table.insert(shipGroup_shipid_table[shipGroup], shipId)
        end
      end
    end
    for shipGroup, skinId in pairs(shipGroup_skinid_table) do
      for k, shipId in pairs(shipGroup_shipid_table[shipGroup]) do
        if (math.floor((shipId % 100000) / 1000) ~= 99) or (shipId % 10 ~= 1) then -- check if ship is PR
          pg.ship_data_statistics[shipId].skin_id = skinId
        end
      end
    end
  end
end

local function fix_skin_word(skinSettings)
  if skinSettings ~= nil then
    for skinId, v in pairs(skinSettings) do
      local shipGroup = pg.ship_skin_template[skinId].ship_group
      for k, v in pairs(pg.ship_skin_words[skinId]) do
        if v == "" or (type(v) == "table" and v[1] == nil) then
          pg.ship_skin_words[skinId][k] = pg.ship_skin_words[shipGroup * 10][k]
        end
      end
    end
  end
end

-- Main Program --
local dataFile = mod_config_path .. "skin.ini"
local skinSettings_pre = read_skinData(dataFile)
local run = generate_skinData(dataFile, skinSettings_pre)
skinSettings_pre = nil
local skinSettings = read_skinData(dataFile)
local applyskin = apply_skin(skinSettings)
local fixskinword = fix_skin_word(skinSettings)

-- Show BD Skins
for k, v in pairs(pg.ship_skin_template) do
  if tostring(k) ~= "all" then
    v.skin_type = -1
  end
end